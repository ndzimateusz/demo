package com.decerto.task.demo.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 */
@Data
@NoArgsConstructor
@Entity
public class Number implements Serializable {
    @Id
    @SequenceGenerator(name = "number_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "number_id_seq")
    private Long numberId;
    private Integer number;
}
