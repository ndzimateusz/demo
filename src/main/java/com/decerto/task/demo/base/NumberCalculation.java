package com.decerto.task.demo.base;

public interface NumberCalculation {

    <T extends Number> T calculate();
}
