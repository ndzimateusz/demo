package com.decerto.task.demo.source;

import com.decerto.task.demo.source.base.DataSource;
import com.decerto.task.demo.exception.DemoException;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Optional;

import static com.decerto.task.demo.exception.DemoExceptionMessages.REST_SERVICE_ERROR;

@Service
public class RestServiceSource implements DataSource {

    private final RestTemplate restTemplate;

    public RestServiceSource(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public Integer getData() {
        String url = "http://www.randomnumberapi.com/api/v1.0/random?min=100&max=1000&count=1";

        try {
            ResponseEntity<Integer[]> response = this.restTemplate.getForEntity(url, Integer[].class);
            return handleResponse(response);
        } catch (HttpStatusCodeException ex) {
            throw new DemoException(REST_SERVICE_ERROR);
        }
    }

    private Integer handleResponse(ResponseEntity<Integer[]> response) {
        return Optional.ofNullable(response.getBody())
                .map(Arrays::asList)
                .get()
                .stream()
                .findFirst()
                .orElse(0);
    }
}
