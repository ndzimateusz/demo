package com.decerto.task.demo.source;

import com.decerto.task.demo.source.base.DataSource;
import com.decerto.task.demo.entity.Number;
import com.decerto.task.demo.service.NumberService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DatabaseSource implements DataSource {

    private final NumberService numberService;

    @Override
    public Integer getData() {
        return numberService.getFirst()
                .map(Number::getNumber)
                .orElse(0);

    }
}
