package com.decerto.task.demo.source.base;

public interface DataSource {
    <T> T getData();
}
