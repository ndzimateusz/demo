package com.decerto.task.demo.repository;

import com.decerto.task.demo.entity.Number;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NumberRepository extends JpaRepository<Number, Long> {

}
