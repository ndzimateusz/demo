package com.decerto.task.demo.exception;

public class DemoExceptionMessages {

    public static final String REST_SERVICE_ERROR = "Wystąpił błąd podczas pobierania danych z serwisu";
    public static final String WRONG_DATA_TYPE = "Źródło zawiera nieprawidłowy typ danych. Dozwolony typ danych to %s";
}
