package com.decerto.task.demo.exception;


public class DemoException extends RuntimeException {

    public DemoException(String messageKey) {
        super(messageKey);
    }

}
