package com.decerto.task.demo.service;

import com.decerto.task.demo.base.NumberCalculation;
import com.decerto.task.demo.exception.DemoException;
import com.decerto.task.demo.source.base.DataSource;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.decerto.task.demo.exception.DemoExceptionMessages.WRONG_DATA_TYPE;

@Service
@RequiredArgsConstructor
public class CalculateAddNumberService implements NumberCalculation {

    private Logger logger = LoggerFactory.getLogger(CalculateAddNumberService.class);

    private final List<? extends DataSource> numbersDataSource;

    @Override
    public Double calculate() {
        try {
            return numbersDataSource.stream()
                    .mapToDouble(DataSource::getData
                    ).sum();
        }  catch (ClassCastException e) {
            throw new DemoException(String.format(WRONG_DATA_TYPE, "liczby"));
        }
    }

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        logger.info(calculate().toString());
    }
}
