package com.decerto.task.demo.service;

import com.decerto.task.demo.entity.Number;
import com.decerto.task.demo.repository.NumberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class NumberService {

    private final NumberRepository numberRepository;

    @Transactional(readOnly = true)
    public Optional<Number> getFirst() {
        return numberRepository.findAll().stream()
                .findFirst();
    }

}
